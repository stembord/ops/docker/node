#!/bin/bash

source .envrc

function build_and_push() {
  node_version=$1
  docker build --build-arg=${node_version} -t ${REGISTRY}/build-agent/node:${node_version} .
  docker push ${REGISTRY}/build-agent/node:${node_version}
}

build_and_push "10"