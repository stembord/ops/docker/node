# Build Agent Node

```bash
cp .envrc.example .envrc
# change any necessary variables
# if direnv is installed
direnv allow
# if not
source .envrc
```

```bash
./build-and-push.sh
```