# build-agent/node:${NODE_VERSION}

ARG NODE_VERSION=10
FROM node:${NODE_VERSION}

ARG DOCKER_VERSION=18.03.1-ce
ARG HELM_VERSION=2.14.1
ARG HELM_PUSH_URL=https://github.com/chartmuseum/helm-push

RUN curl -fsSLO https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKER_VERSION}.tgz 
RUN tar xzvf docker-${DOCKER_VERSION}.tgz --strip 1 -C /usr/local/bin docker/docker 
RUN rm docker-${DOCKER_VERSION}.tgz 
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/local/bin/kubectl
RUN curl -s https://storage.googleapis.com/kubernetes-helm/helm-v${HELM_VERSION}-linux-amd64.tar.gz -o helm.tar.gz
RUN tar xf helm.tar.gz
RUN mv linux-amd64/helm /usr/local/bin/
RUN rm -rf linux-amd64 
RUN rm helm.tar.gz
RUN apt install git -y
RUN helm init --client-only
RUN helm plugin install ${HELM_PUSH_URL}

ENV HELM_HOST tiller-deploy.kube-system.svc.cluster.local:44134